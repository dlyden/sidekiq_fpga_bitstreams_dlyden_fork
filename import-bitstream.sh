#!/bin/bash

docker run --rm -it -u $(id -u):$(id -g) -v $PWD:/work -w /work docker.epiq.rocks/skiq-import-bitstream:latest python3 import-bitstream.py "$@"
