#!/usr/bin/env python3

#
# example usage:
#  python3 import-bitstream.py \
#     --url https://jenkins.epiq.rocks/job/Sidekiq/job/FPGAPlatformSpecific/job/sidekiq-x-fpga/250/ \
#     --product x4-pdk \
#     --version 3.15.0
#
#  python3 import-bitstream.py \
#     --url https://jenkins.epiq.rocks/job/Sidekiq/job/FPGAPlatformSpecific/job/sidekiq-x-fpga/250/ \
#     --product 'x*' \
#     --version 3.15.0
#

import yaml
import zipfile
import re
import os.path
import os, errno
import glob
import subprocess
import urllib.request

from tqdm import tqdm

M_SERIES = ['m2-pcie','m2-usb','m2-2280']
X_SERIES = ['x2-pdk','x2-pdk-ku115','x4-pdk','x4-pdk-ku115','x4-pdk-htgk810','x4-pdk-htgk810-ku115','x4-pdk-xbm' ]
Z_SERIES = ['z2','z2p','z3u']

# From https://stackoverflow.com/a/53877507 -- Python 3 with TQDM
class DownloadProgressBar(tqdm):
    def update_to(self, b=1, bsize=1, tsize=None):
        if tsize is not None:
            self.total = tsize
        self.update(b * bsize - self.n)


def download_url(url, output_path):
    with DownloadProgressBar(unit='B', unit_scale=True,
                             miniters=1, desc=url.split('/')[-1]) as t:
        urllib.request.urlretrieve(url, filename=output_path, reporthook=t.update_to)

# from https://stackoverflow.com/questions/8299386/modifying-a-symlink-in-python
def symlink_force(target, link_name):
    try:
        os.symlink(target, link_name)
    except OSError as e:
        if e.errno == errno.EEXIST:
            os.remove(link_name)
            os.symlink(target, link_name)
        else:
            raise e

def whee(data, url, version):
    git_cmds = []

    build = data['url-name-prefix']
    archive_url = f'{url}/artifact/.output/build_{build}/*zip*/build_{build}.zip'
    fn = f'build_{build}.zip'
    print(archive_url)

    if not os.path.isfile(fn):
        download_url(archive_url, fn)
    else:
        print(fn + " exists, skipping download")

    zf = zipfile.ZipFile(fn)

    p = re.compile(f'{build}' + r'_[0-9A-Fa-f]{8}_[0-9A-Fa-f]{8}_v[^.]*\.bi[nt]')
    candidates = filter(lambda x: p.search(x.filename), zf.filelist)
    bitstream = None
    for candidate in candidates:
        if candidate.filename.endswith(data['symlink'][-4:]):
            bitstream = candidate

    # This is the case for the Z2 bitstream(s), they don't have a githash and build date in their
    # names
    if bitstream is None:
        p = re.compile(f'{build}' + r'_v[^.]*\.bi[nt]')
        candidates = filter(lambda x: p.search(x.filename), zf.filelist)
        for candidate in candidates:
            if candidate.filename.endswith(data['symlink'][-4:]):
                bitstream = candidate

    if bitstream:
        bitstream.filename = 'pdk/' + os.path.basename(bitstream.filename);
        symlink_filename = data['symlink'].replace('VERSION', version)

        # clean up existing clutter
        p = re.compile(data['symlink'].replace('VERSION', r'.*'))
        for name in os.listdir('.'):
            if p.match(name):
                realname = os.readlink(name)

                if symlink_filename != name:
                    print(f"Removing {name}")
                    git_cmds.append(f"git rm {name}")
                if bitstream.filename != realname:
                    print(f"Removing {realname}")
                    git_cmds.append(f"git rm {realname}")

        print(f"Extracting {bitstream.filename}")
        zf.extract(bitstream)
        git_cmds.append(f"git add {bitstream.filename}")

        print(f"Creating symlink {symlink_filename}")
        symlink_force(bitstream.filename, symlink_filename)
        git_cmds.append(f"git add {symlink_filename}")

    return git_cmds

if __name__ == '__main__':
    import argparse, sys

    parser = argparse.ArgumentParser(description='import FPGA bitstreams into the repo')
    parser.add_argument('--url', type=str, required=True, help='URL of the Jenkins job from which to import artifacts')
    parser.add_argument('--version', type=str, required=True, help='version string (without the v)')
    parser.add_argument('--product', type=str, required=True, help='comma delimited list of products, or "m*", "x*", "z*" to specify a series')

    args = parser.parse_args(sys.argv[1:])

    with open('bitstreams.yml') as stream:
        data = yaml.safe_load(stream)

    url = args.url
    version = args.version
    products = args.product.strip()
    if products == 'm*':
        products = M_SERIES
    elif products == 'x*':
        products = X_SERIES
    elif products == 'z*':
        products = Z_SERIES
    else:
        products = products.split(',')

    GIT = []
    for product in products:
        GIT += whee(data[product], url, version)

    print("Run these git commands to complete the transaction:\n")
    print('\n'.join(GIT) + '\n')
